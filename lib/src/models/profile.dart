// To parse this JSON data, do
//
//     final profile = profileFromJson(jsonString);

import 'dart:convert';

Profile profileFromJson(String str) => Profile.fromJson(json.decode(str));

String profileToJson(Profile data) => json.encode(data.toJson());

class Profile {
  final String personCode;
  final String title;
  final String personName;
  final String positionName;
  final String locationName;
  final String orgUnitName;

  Profile({
    required this.personCode,
    required this.title,
    required this.personName,
    required this.positionName,
    required this.locationName,
    required this.orgUnitName,
  });

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
        personCode: json["PersonCode"],
        title: json["Title"],
        personName: json["PersonName"],
        positionName: json["PositionName"],
        locationName: json["LocationName"],
        orgUnitName: json["OrgUnitName"],
      );

  Map<String, dynamic> toJson() => {
        "PersonCode": personCode,
        "Title": title,
        "PersonName": personName,
        "PositionName": positionName,
        "LocationName": locationName,
        "OrgUnitName": orgUnitName,
      };
}
