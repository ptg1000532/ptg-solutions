// To parse this JSON data, do
//
//     final workTime = workTimeFromJson(jsonString);

import 'dart:convert';

import 'package:equatable/equatable.dart';

List<WorkTime> allWorkTimeFromJson(String str) =>
    List<WorkTime>.from(json.decode(str).map((x) => WorkTime.fromJson(x)));

WorkTime workTimeFromJson(String str) => WorkTime.fromJson(json.decode(str));

String workTimeToJson(WorkTime data) => json.encode(data.toJson());

class WorkTime extends Equatable {
  final String? empCode;
  final DateTime? workDate;
  final String? orgUnitCode;
  final DateTime? workTimeIn;
  final DateTime? workTimeOut;
  final String? cardTimeIn;
  final String? cardTimeOut;

  WorkTime({
    this.empCode,
    this.workDate,
    this.orgUnitCode,
    this.workTimeIn,
    this.workTimeOut,
    this.cardTimeIn,
    this.cardTimeOut,
  });

  factory WorkTime.fromJson(Map<String, dynamic> json) => WorkTime(
        empCode: json["EmpCode"],
        workDate:
            json["WorkDate"] == null ? null : DateTime.parse(json["WorkDate"]),
        orgUnitCode: json["OrgUnitCode"],
        workTimeIn: json["WorkTimeIN"] == null
            ? null
            : DateTime.parse(json["WorkTimeIN"]),
        workTimeOut: json["WorkTimeOut"] == null
            ? null
            : DateTime.parse(json["WorkTimeOut"]),
        cardTimeIn: json["CardTimeIn"],
        cardTimeOut: json["CardTimeOut"],
      );

  Map<String, dynamic> toJson() => {
        "EmpCode": empCode,
        "WorkDate": workDate,
        "OrgUnitCode": orgUnitCode,
        "WorkTimeIN": workTimeIn,
        "WorkTimeOut": workTimeOut,
        "CardTimeIn": cardTimeIn,
        "CardTimeOut": cardTimeOut,
      };

  @override
  List<Object?> get props => [
        cardTimeIn,
        cardTimeOut,
      ];
}
