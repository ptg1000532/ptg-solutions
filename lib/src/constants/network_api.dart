class NetworkAPI {
  static const String baseURL = 'http://192.168.100.118:3000';
  static const String loginEndpoint = '/login';
  static const String profileEndpoint = '/getProfile';
  static const String getTodayWorkTimeEndpoint = '/getTodayClockActivity';
  static const String getAllWorkTimeEndpoint = '/getAllClockActivity';
  static const String getMonthWorkTimeEndpoint = '/getMonthClockActivity';
  static const String saveWorkTimeEndpoint = '/saveWorkTime';
  static const String imageURL = '$baseURL/images';
  static const String username = 'username';
  static const String personCode = 'personCode';
}
