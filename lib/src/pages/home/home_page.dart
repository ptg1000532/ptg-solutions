// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:ptg_solutions/src/app.dart';
import 'package:ptg_solutions/src/bloc/attendance/attendance_bloc.dart';
import 'package:ptg_solutions/src/bloc/auth/auth_bloc.dart';
import 'package:ptg_solutions/src/constants/asset.dart';
import 'package:ptg_solutions/src/models/work_time.dart';
import 'package:ptg_solutions/src/pages/app_router.dart';
import 'package:ptg_solutions/src/pages/home/widgets/RelativeTimestampWidget.dart';
import 'package:ptg_solutions/src/utils/common_utils.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Location location = Location();
  LocationData? lastLocation;
  bool isTargetLocation = false;
  String pushToken = "";

  @override
  void initState() {
    super.initState();
    loadProfile();
    _getLastLocation();
    _setupNotification();
    _setupCrashlytic();
  }

  void _setupNotification() {
    FirebaseMessaging.instance.getToken().then((value) {
      pushToken = value.toString();
      print("Push Token: " + value.toString());
      setState(() {});
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print("message recieved");
      print(event.notification!.body);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Notification"),
              content: Text(event.notification!.body!),
              actions: [
                TextButton(
                  child: const Text("Ok"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          });
    });
  }

  Future<void> _getLastLocation() async {
    bool serviceEnabled = await location.serviceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return;
      }
    }

    PermissionStatus permissionStatus = await location.hasPermission();
    if (permissionStatus == PermissionStatus.denied) {
      permissionStatus = await location.requestPermission();
      if (permissionStatus != PermissionStatus.granted) {
        return;
      }
    }

    LocationData? currentLocation = await location.getLocation();
    setState(() {
      lastLocation = currentLocation;
    });
  }

  loadProfile() async {
    context.read<AuthBloc>().add(AuthEventGetProfile());
    context.read<AttendanceBloc>().add(AttendanceEventLoadClockWork());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const CustomDrawer(),
      appBar: AppBar(
        title: Text("PTG SOLUTIONS"),
        actions: [IconButton(onPressed: _reload(), icon: Icon(Icons.refresh))],
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          top: 50,
          bottom: 90,
          left: 20,
          right: 20,
        ),
        child: BlocListener<AttendanceBloc, AttendanceState>(
          listenWhen: (previous, current) {
            return (current.resultMessage != null && previous.resultMessage != current.resultMessage);
          },
          listener: (context, state) {
            if (state.resultMessage != null) {
              // update work time
              context.read<AttendanceBloc>().add(AttendanceEventLoadClockWork());

              if (state.resultMessage != "Loading..") {
                final title = state.workTime!.cardTimeOut != null ? "ลงเวลาออกงานสำเร็จ" : "ลงเวลาเข้างานสำเร็จ";

                _showSuccessDialog(title: title, workTime: state.workTime!);
              }
            }
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _buildEmployeeProfile(),
              SizedBox(height: 10),
              _buildUserCurrentLocation(),
              const Spacer(),
              _buildEmployeeCheckIn(),
            ],
          ),
        ),
      ),
    );
  }

  _buildEmployeeProfile() {
    const _spacing = 10.0;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _buildProfileImage(),
        SizedBox(height: _spacing),
        _buildProfileInfo(),
      ],
    );
  }

  _buildEmployeeCheckIn() {
    return BlocBuilder<AttendanceBloc, AttendanceState>(
      builder: (context, state) {
        final isWorkInActive = (state.workTime?.cardTimeIn == null);
        final isAlreadyWorkOut = (state.workTime?.cardTimeIn != null && state.workTime?.cardTimeOut != null);

        return Card(
            color: Colors.grey.shade100,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: SizedBox(
                //height: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    // Left
                    RelativeTimestampWidget(),
                    // Right
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              primary: Colors.black87,
                              side: BorderSide(
                                color: Colors.transparent,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, AppRoute.report);
                            },
                            child: Row(
                              children: [
                                Text(
                                  "ตรวจสอบเวลางาน",
                                ),
                                const Icon(Icons.chevron_right)
                              ],
                            )),
                        //Spacer(),
                        ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(isWorkInActive
                                ? Colors.blue
                                : isAlreadyWorkOut
                                    ? Colors.grey
                                    : Colors.red),
                          ),
                          onPressed: (isAlreadyWorkOut == true || !isTargetLocation)
                              ? null
                              : () {
                                  context.read<AttendanceBloc>().add(AttendanceEventSaveClockWork());
                                },
                          child: Text(isWorkInActive ? "ลงเวลาเข้างาน" : "ลงเวลาเลิกงาน"),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ));
      },
    );
  }

  _buildProfileImage() => Container(
        height: 170,
        width: 170,
        decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20),
            border: Border.all(
              color: Colors.grey.shade100,
              width: 1.5,
            )),
        child: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            if (state.profile == null) {
              return Text("Loading..");
            } else {
              return FadeInImage(
                image: NetworkImage("https://wn2013.ptg.co.th/Upload/Employee/${state.profile?.personCode}.jpg"),
                placeholder: const AssetImage(Asset.cmLogoImage),
                imageErrorBuilder: (context, error, stackTrace) {
                  return Image.asset(Asset.elonImage, fit: BoxFit.fitWidth);
                },
                fit: BoxFit.fitWidth,
              );
            }
          },
        ),
      );

  _buildProfileInfo() {
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        return Column(
          children: [
            // PersonCode
            Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 3),
              child: Text(state.profile?.personCode ?? "Loading..",
                  style: TextStyle(
                    color: Colors.red[700],
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  )),
            ),
            // Full Name
            Padding(
              padding: const EdgeInsets.only(bottom: 3),
              child: Text(state.profile?.personName ?? "Loading..",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                  )),
            ),
            SizedBox(height: 8),
            // Position
            Text(state.profile?.positionName ?? "Loading..",
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                )),
            SizedBox(height: 6),
            // Position
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Location Name
                Text(state.profile?.locationName ?? "Loading..",
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                    )),
                Padding(
                  padding: EdgeInsets.only(top: 4, left: 8, right: 8),
                  child: Container(
                    height: 16,
                    width: 1,
                    color: Colors.black87.withOpacity(0.8),
                  ),
                ),
                // orgUnitName
                Text(state.profile?.orgUnitName ?? "Loading..",
                    style: TextStyle(
                      color: Colors.black87,
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                    ))
              ],
            )
          ],
        );
      },
    );
  }

  void _showSuccessDialog({required title, required WorkTime workTime}) {
    final workInTime = (workTime.cardTimeIn == null) ? "-" : formatTime(timestamp: workTime.cardTimeIn!, newFormat: "HH:mm น.");

    final workOutTime = (workTime.cardTimeOut == null) ? "-" : formatTime(timestamp: workTime.cardTimeOut!, newFormat: "HH:mm น.");

    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          content: Container(
              height: workOutTime != "-" ? 340 : 310,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 15.0, top: 15),
                    child: Container(
                      height: 110,
                      width: 110,
                      decoration: BoxDecoration(
                        color: Colors.blue.shade100,
                        shape: BoxShape.circle,
                      ),
                      child: const Icon(
                        Icons.check_rounded,
                        color: Colors.blue,
                        size: 60,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5.0, left: 30, right: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [Text("เวลาเข้า:", style: _timeStyle(color: Colors.green.shade400)), Text(workInTime, style: _timeStyle(color: Colors.green.shade400))],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5.0, left: 30, right: 30),
                    child: workOutTime != "-"
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [Text("เวลาออก:", style: _timeStyle(color: Colors.green.shade400)), Text(workOutTime, style: _timeStyle(color: Colors.green.shade400))],
                          )
                        : null,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(title, style: _timeStyle()),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    width: 120,
                    child: ElevatedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("ตกลง")),
                  )
                ],
              )),
        );
      },
    );
  }

  _timeStyle({Color color = Colors.black}) {
    return TextStyle(
      color: color,
      fontSize: 20,
      fontWeight: FontWeight.w700,
    );
  }

  bool _isLocationWithinBoundary(double boundaryRadiusMeters) {
    if (lastLocation == null) {
      return false;
    }

    double distanceInMeters = Geolocator.distanceBetween(
      lastLocation!.latitude!,
      lastLocation!.longitude!,
      8.1554,
      99.7006,
    );

    return distanceInMeters <= boundaryRadiusMeters;
  }

  _buildUserCurrentLocation() {
    if (lastLocation != null) {
      final lat = formatLocation.format(lastLocation!.latitude);
      final lng = formatLocation.format(lastLocation!.longitude);

      isTargetLocation = _isLocationWithinBoundary(50);

      return Column(
        children: [
          Text("ตำแหน่งปัจจุบัน: $lat, $lng", textAlign: TextAlign.center),
          if (!isTargetLocation)
            Text(
              "คุณไม่ได้อยู่ที่บริษัท",
              style: TextStyle(color: Colors.red),
            )
        ],
      );
    } else {
      return Text("กำลังค้นหาตำแหน่งปัจจุบัน..");
    }
  }

  _reload() {
    _getLastLocation();
    context.read<AttendanceBloc>().add(AttendanceEventLoadClockWork());
  }

  Future<void> _setupCrashlytic() async {
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  }
}

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          _buildProfile(),
          ListTile(
            onTap: _wantToCrash,
            title: const Text("Simulate Crash"),
            leading: const Icon(Icons.dangerous, color: Colors.red),
          ),
          const Spacer(),
          _buildLogoutButton(),
        ],
      ),
    );
  }

  UserAccountsDrawerHeader _buildProfile() => UserAccountsDrawerHeader(
        currentAccountPicture: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            shape: BoxShape.circle,
          ),
          child: const CircleAvatar(
            backgroundImage: AssetImage(Asset.cmLogoImage),
          ),
        ),
        accountName: const Text('CMDev'),
        accountEmail: const Text('support@codemobiles.com'),
      );

  Builder _buildLogoutButton() => Builder(
        builder: (context) => SafeArea(
          child: ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: const Text('Log out'),
            onTap: () => context.read<AuthBloc>().add(AuthEventLogout()),
          ),
        ),
      );

  _wantToCrash() {
    // FirebaseCrashlytics.instance.crash();
    //
    final array = [1, 2, 3];
    print(array[3]); // this will cause index-out-of-bound error
  }
}
