import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class RelativeTimestampWidget extends StatefulWidget {
  @override
  _RelativeTimestampWidgetState createState() =>
      _RelativeTimestampWidgetState();
}

class _RelativeTimestampWidgetState extends State<RelativeTimestampWidget> {
  late Timer _timer;
  late String relativeTime;
  String? timeText;
  String? dayText;

  @override
  void initState() {
    super.initState();
    _updateRelativeTime();
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      _updateRelativeTime();
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void _updateRelativeTime() {
    final currentTime = DateTime.now();
    final timeFormat = DateFormat.Hm();
    final dateFormat = DateFormat('dd/MM/yy');

    timeText = timeFormat.format(currentTime);
    dayText = _formatDay(dateFormat.format(currentTime));

    setState(() {});
  }

  _formatDay(String source) {
    final dd = source.split("/")[0];
    final mm = _getThaiMM(int.parse(source.split("/")[1]));
    final yy = int.parse(source.split("/")[2]) + 43;

    return "${_getThaiDay()} $dd $mm $yy";
  }

  _getThaiMM(int index) {
    switch (index) {
      case 1:
        return "ม.ค.";
      case 2:
        return "ก.พ.";
      case 3:
        return "มี.ค.";
      case 4:
        return "เม.ย.";
      case 5:
        return "พ.ค.";
      case 6:
        return "มิ.ย.";
      case 7:
        return "ก.ค.";
      case 8:
        return "ส.ค.";
      case 9:
        return "ก.ย.";
      case 10:
        return "ต.ค.";
      case 11:
        return "พ.ย.";
      case 12:
        return "ธ.ค.";
      default:
    }
  }

  _getThaiDay() {
    switch (DateTime.now().weekday) {
      case 7:
        return "อา.";
      case 1:
        return "จ.";
      case 2:
        return "อ.";
      case 3:
        return "พ.";
      case 4:
        return "พฤ.";
      case 5:
        return "ศ.";
      case 6:
        return "ส.";
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Text("ลงเวลาทำงาน",
            style: TextStyle(
              color: Colors.black87,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            )),
        Text("$timeText น." ?? "",
            style: const TextStyle(
              color: Colors.black87,
              fontSize: 23,
              fontWeight: FontWeight.bold,
            )),
        Text(dayText ?? "",
            style: const TextStyle(
              color: Colors.black54,
              fontSize: 17,
              fontWeight: FontWeight.w400,
            )),
      ],
    );
  }
}
