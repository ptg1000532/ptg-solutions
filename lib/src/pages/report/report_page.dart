import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ptg_solutions/src/bloc/attendance/attendance_bloc.dart';
import 'package:ptg_solutions/src/utils/common_utils.dart';

class ReportPage extends StatefulWidget {
  const ReportPage({super.key});

  @override
  State<ReportPage> createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage> {
  @override
  void initState() {
    super.initState();
    context.read<AttendanceBloc>().add(AttendanceEventLoadAllClockWork());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Report'),
      ),
      body: BlocListener<AttendanceBloc, AttendanceState>(
        listenWhen: (previous, current) {
          return (previous.selectedDate != current.selectedDate);
        },
        listener: (context, state) {
          context.read<AttendanceBloc>().add(
                AttendanceEventLoadAllClockWork(selectedDate: state.selectedDate),
              );
        },
        child: Column(children: [
          _buildMonthSelection(),
          _buildHeaderColumn(),
          Expanded(
            child: RefreshIndicator(
              onRefresh: () async => context.read<AttendanceBloc>().add(AttendanceEventLoadAllClockWork()),
              child: _buildListView(),
            ),
          ),
        ]),
      ),
    );
  }

  _buildHeaderColumn() {
    return Container(
      padding: const EdgeInsets.all(10),
      color: Colors.grey.shade100,
      child: Row(
        children: [
          Text("วันที่", style: _buildColumnTextStyle()),
          const Spacer(),
          const Spacer(),
          Text("เข้า", style: _buildColumnTextStyle()),
          const Spacer(),
          Text("ออก", style: _buildColumnTextStyle()),
          const Spacer(),
        ],
      ),
    );
  }

  _buildColumnTextStyle({double fontSize = 16}) {
    return TextStyle(
      fontSize: fontSize,
    );
  }

  _buildListView() {
    return BlocBuilder<AttendanceBloc, AttendanceState>(
      builder: (context, state) {
        return state.allWorkTime.isEmpty
            ? const Center(child: Text("Loading.."))
            : Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView.builder(
                  itemCount: state.allWorkTime.length,
                  itemBuilder: (context, index) {
                    final worktime = state.allWorkTime[index];

                    // work date
                    final workInDate = worktime.workDate == null ? "-" : formatDate(dateTime: worktime.workDate!);

                    // work in
                    final workInTime = worktime.cardTimeIn == null ? "-" : "${formatTime(timestamp: worktime.cardTimeIn!, newFormat: "HH:mm")} น.";

                    // work out
                    final workOutTime = worktime.cardTimeOut == null ? "-" : "${formatTime(timestamp: worktime.cardTimeOut!, newFormat: "HH:mm")} น.";

                    return Row(
                      children: [
                        Text(workInDate, style: _buildColumnTextStyle(fontSize: 20)),
                        const Spacer(),
                        const Spacer(),
                        _buildTimeText(workInTime),
                        const Spacer(),
                        _buildTimeText(workOutTime),
                        const Spacer(),
                      ],
                    );
                  },
                ),
              );
      },
    );
  }

  _buildTimeText(String workInTime) {
    return SizedBox(
      width: 90,
      child: Text(
        workInTime,
        style: _buildColumnTextStyle(),
      ),
    );
  }

  _buildMonthSelection() {
    return BlocBuilder<AttendanceBloc, AttendanceState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              IconButton(
                  onPressed: () {
                    context.read<AttendanceBloc>().add(AttendanceEventBackwardSelectedDateClockWork());
                  },
                  icon: Icon(Icons.arrow_back_ios_new_rounded)),
              Spacer(),
              Text(
                formatDate(dateTime: state.selectedDate, newFormat: "MMM yyyy"),
                style: _buildColumnTextStyle(),
              ),
              Spacer(),
              IconButton(
                  onPressed: () {
                    context.read<AttendanceBloc>().add(AttendanceEventForwardSelectedDateClockWork());
                  },
                  icon: Icon(Icons.arrow_forward_ios_rounded)),
            ],
          ),
        );
      },
    );
  }
}
