// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ptg_solutions/src/bloc/auth/auth_bloc.dart';
import 'package:ptg_solutions/src/constants/asset.dart';
import 'package:ptg_solutions/src/models/user.dart';
import 'package:ptg_solutions/src/pages/app_router.dart';
import 'package:ptg_solutions/widgets/custom_flushbar.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _usernameController.text = "AEKACHAI";
    _passwordController.text = "000304";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(top: 70),
        child: BlocListener<AuthBloc, AuthState>(
          listenWhen: (previous, current) {
            return previous.status != current.status;
          },
          listener: (context, state) {
            if (state.status == LoginStatus.success) {
              Navigator.pushNamed(context, AppRoute.home);
            } else if (state.status == LoginStatus.failed) {
              CustomFlushbar.showError(context, message: "Login failed");
            }
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // Header
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: SizedBox(height: 100, child: Image.asset(Asset.ptgLogoImage)),
              ),
              // Form
              _buildForm(),
            ],
          ),
        ),
      ),
    );
  }

  _buildForm() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Card(
        elevation: 0,
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // Username
              TextField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'codemobiles@gmail.com',
                    labelText: 'Username',
                    icon: Icon(Icons.email),
                  )),

              // Password
              TextField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: '',
                    labelText: 'Password',
                    icon: Icon(Icons.lock),
                  )),

              // Login Button
              ElevatedButton(onPressed: _login, child: Text("Login")),
              // Register Button
              OutlinedButton(onPressed: _register, child: Text("Register"))
            ],
          ),
        ),
      ),
    );
  }

  void _login() {
    final user = User(
      _usernameController.text,
      _passwordController.text,
    );

    context.read<AuthBloc>().add(AuthEventLogin(user));
  }

  void _register() {
    // Navigator.pushNamed(context, "register");

    final user = User(
      _usernameController.text,
      _passwordController.text,
    );

    context.read<AuthBloc>().add(AuthEventRegister(user));
  }
}
