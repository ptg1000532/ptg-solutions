import 'package:flutter/material.dart';
import 'package:ptg_solutions/src/pages/loading/loading_page.dart';
import 'package:ptg_solutions/src/pages/report/report_page.dart';

import 'home/home_page.dart';
import 'login/login_page.dart';

class AppRoute {
  static const loading = 'loading';
  static const home = 'home';
  static const report = 'report';
  static const login = 'login';

  static get all => <String, WidgetBuilder>{
        loading: (context) => const LoadingPage(),
        login: (context) => const LoginPage(),
        home: (context) => const HomePage(),
        report: (context) => const ReportPage(),
      };
}
