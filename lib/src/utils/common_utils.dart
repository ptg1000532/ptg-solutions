import 'package:intl/intl.dart';

String formatTime({required String timestamp, String? newFormat}) {
  // Parse the timestamp and format it as HH:mm
  try {
    DateTime dateTime = DateFormat("yyyy-MM-ddTHH:mm:ss.SSSZ").parse(timestamp);
    // Set the time zone offset for Bangkok (UTC+7)
    DateTime thaiTime = dateTime.add(const Duration(hours: 7));
    String formattedTime = DateFormat(
      newFormat ?? 'HH:mm',
    ).format(thaiTime);

    return formattedTime;
  } catch (e) {
    return timestamp;
  }
}

String formatDate({required DateTime dateTime, String? newFormat}) {
  // Parse the timestamp and format it as HH:mm
  try {
    // Set the time zone offset for Bangkok (UTC+7)
    DateTime thaiTime = dateTime.add(const Duration(hours: 7));
    String formattedDate = DateFormat(newFormat ?? 'dd/MM/yy').format(thaiTime);

    return formattedDate;
  } catch (e) {
    return "Error";
  }
}
