import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:ptg_solutions/src/bloc/attendance/attendance_bloc.dart';
import 'package:ptg_solutions/src/constants/network_api.dart';
import 'package:ptg_solutions/src/pages/app_router.dart';
import 'package:ptg_solutions/src/pages/home/home_page.dart';
import 'package:ptg_solutions/src/pages/loading/loading_page.dart';
import 'package:ptg_solutions/src/pages/login/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bloc/auth/auth_bloc.dart';

final formatTimeHHmm = DateFormat('HH:mm');
final formatLocation = NumberFormat('###.000');
final navigatorState = GlobalKey<NavigatorState>();

class App extends StatelessWidget {
  App({super.key});

  final authBloc = BlocProvider(create: (context) => AuthBloc());
  final attendanceBloc = BlocProvider(create: (context) => AttendanceBloc());

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        authBloc,
        attendanceBloc,
      ],
      child: MaterialApp(
        title: "PTG-Solutions",
        navigatorKey: navigatorState,
        routes: AppRoute.all,
        home: _initailPage(),
        theme: _buildTheme(Brightness.light),
      ),
    );
  }

  ThemeData _buildTheme(brightness) {
    var baseTheme = ThemeData(brightness: brightness);

    return baseTheme.copyWith(
      textTheme: GoogleFonts.notoSansThaiTextTheme(baseTheme.textTheme),
    );
  }

  _initailPage() {
    return FutureBuilder(
      future: SharedPreferences.getInstance(),
      builder: (context, snapshot) {
        if (snapshot.hasData == false) {
          return const LoadingPage();
        }

        final prefs = snapshot.data!;
        final personCode = prefs.getString(NetworkAPI.personCode);
        return personCode != null ? HomePage() : LoginPage();
      },
    );
  }
}
