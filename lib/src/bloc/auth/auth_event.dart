part of 'auth_bloc.dart';

sealed class AuthEvent {
  const AuthEvent();
}

class AuthEventLogin extends AuthEvent {
  final User payload;

  AuthEventLogin(this.payload);
}

class AuthEventRegister extends AuthEvent {
  final User payload;

  AuthEventRegister(this.payload);
}

class AuthEventLogout extends AuthEvent {}

class AuthEventGetProfile extends AuthEvent {}
