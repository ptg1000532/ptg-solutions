part of 'auth_bloc.dart';

enum LoginStatus { fetching, success, failed, init }

class AuthState extends Equatable {
  const AuthState({
    required this.status,
    this.profile,
  });
  final LoginStatus status;

  final Profile? profile;

  AuthState copyWith({
    LoginStatus? status,
    User? user,
    Profile? profile,
  }) {
    return AuthState(
      status: status ?? this.status,
      profile: profile ?? this.profile,
    );
  }

  @override
  List<Object?> get props => [
        status,
        profile,
      ];
}
