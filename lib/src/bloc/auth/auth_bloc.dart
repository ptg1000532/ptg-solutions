import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:ptg_solutions/src/app.dart';
import 'package:ptg_solutions/src/constants/network_api.dart';
import 'package:ptg_solutions/src/models/profile.dart';
import 'package:ptg_solutions/src/models/user.dart';
import 'package:ptg_solutions/src/pages/app_router.dart';
import 'package:ptg_solutions/src/services/network_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(const AuthState(status: LoginStatus.init)) {
    // Login
    on<AuthEventLogin>((event, emit) async {
      final prefs = await SharedPreferences.getInstance();
      try {
        emit(state.copyWith(status: LoginStatus.fetching));
        final result = await NetworkService().login(event.payload);
        if (result) {
          emit(state.copyWith(
            status: LoginStatus.success,
            user: event.payload,
          ));

          prefs.setString(NetworkAPI.personCode, event.payload.password);
        } else {
          emit(state.copyWith(status: LoginStatus.failed));
          prefs.remove(NetworkAPI.personCode);
        }
      } catch (e) {
        emit(state.copyWith(status: LoginStatus.failed));
        prefs.remove(NetworkAPI.personCode);
      }
    });

    on<AuthEventRegister>((event, emit) {
      // TBD
    });

    on<AuthEventLogout>((event, emit) async {
      final prefs = await SharedPreferences.getInstance();
      prefs.clear();
      Navigator.pushReplacementNamed(
          navigatorState.currentContext!, AppRoute.login);
    });

    on<AuthEventGetProfile>((event, emit) async {
      try {
        final prefs = await SharedPreferences.getInstance();
        final personCode = prefs.getString(NetworkAPI.personCode);
        final profile = await NetworkService().getProfile(personCode);
        emit(state.copyWith(profile: profile));
      } catch (e) {
        print("Error");
      }
    });
  }
}
