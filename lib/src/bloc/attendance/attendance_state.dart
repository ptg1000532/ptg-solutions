part of 'attendance_bloc.dart';

class AttendanceState extends Equatable {
  const AttendanceState({
    required this.allWorkTime,
    required this.selectedDate,
    this.workTime,
    this.currentPosition,
    this.resultMessage,
  });

  final WorkTime? workTime;
  final List<WorkTime> allWorkTime;
  final LatLng? currentPosition;
  final String? resultMessage;
  final DateTime selectedDate;

  AttendanceState copyWith({
    WorkTime? workTime,
    DateTime? selectedDate,
    List<WorkTime>? allWorkTime,
    LatLng? currentPosition,
    String? resultMessage,
  }) {
    return AttendanceState(
      selectedDate: selectedDate ?? this.selectedDate,
      workTime: workTime ?? this.workTime,
      allWorkTime: allWorkTime ?? this.allWorkTime,
      currentPosition: currentPosition ?? this.currentPosition,
      resultMessage: resultMessage ?? this.resultMessage,
    );
  }

  @override
  List<Object?> get props => [
        selectedDate,
        workTime,
        allWorkTime,
        currentPosition,
        resultMessage,
      ];
}
