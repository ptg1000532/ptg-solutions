part of 'attendance_bloc.dart';

sealed class AttendanceEvent {
  const AttendanceEvent();
}

class AttendanceEventLoadAllClockWork extends AttendanceEvent {
  final DateTime? selectedDate;

  AttendanceEventLoadAllClockWork({this.selectedDate});
}

class AttendanceEventForwardSelectedDateClockWork extends AttendanceEvent {}

class AttendanceEventBackwardSelectedDateClockWork extends AttendanceEvent {}

class AttendanceEventLoadClockWork extends AttendanceEvent {}

class AttendanceEventSaveClockWork extends AttendanceEvent {}
