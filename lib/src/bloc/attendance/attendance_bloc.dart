import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:jiffy/jiffy.dart';
import 'package:ptg_solutions/src/constants/network_api.dart';
import 'package:ptg_solutions/src/models/work_time.dart';
import 'package:ptg_solutions/src/services/network_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'attendance_event.dart';
part 'attendance_state.dart';

class AttendanceBloc extends Bloc<AttendanceEvent, AttendanceState> {
  AttendanceBloc()
      : super(AttendanceState(
          allWorkTime: [],
          selectedDate: DateTime.now(),
        )) {
    // Forward Selected Date
    on<AttendanceEventForwardSelectedDateClockWork>((event, emit) async {
      DateTime newDate = Jiffy.parseFromDateTime(state.selectedDate).add(months: 1).dateTime;
      emit(state.copyWith(selectedDate: newDate));
    });

    // Backward Selected Date
    on<AttendanceEventBackwardSelectedDateClockWork>((event, emit) async {
      DateTime newDate = Jiffy.parseFromDateTime(state.selectedDate).subtract(months: 1).dateTime;
      emit(state.copyWith(selectedDate: newDate));
    });

    // Load Work time
    on<AttendanceEventLoadClockWork>((event, emit) async {
      try {
        final prefs = await SharedPreferences.getInstance();
        final personCode = prefs.getString(NetworkAPI.personCode);
        final todayWorkTime = await NetworkService().getTodayWorkTime(personCode);
        emit(state.copyWith(workTime: todayWorkTime, resultMessage: "Loading.."));
        await Future.delayed(Duration(milliseconds: 100));
        emit(state.copyWith(resultMessage: null));
      } catch (e) {
        print(e.toString());
      }
    });

    // Load All Work time
    on<AttendanceEventLoadAllClockWork>((event, emit) async {
      try {
        emit(state.copyWith(allWorkTime: []));
        final selectedMonth = Jiffy.parseFromDateTime(event.selectedDate ?? DateTime.now()).startOf(Unit.month).dateTime;
        final allWorkTime = await NetworkService().getMonthWorkTime(selectedMonth: selectedMonth);
        emit(state.copyWith(allWorkTime: allWorkTime));
      } catch (e) {
        print(e.toString());
      }
    });

    // Save Work time
    on<AttendanceEventSaveClockWork>((event, emit) async {
      final prefs = await SharedPreferences.getInstance();
      final personCode = prefs.getString(NetworkAPI.personCode);
      final result = await NetworkService().saveWorkTime(
        personCode!,
        "13.232,100.323",
        (state.workTime?.cardTimeIn == null) ? "workin" : "workout",
      );
      emit(state.copyWith(workTime: result));
      await Future.delayed(const Duration(milliseconds: 100));
      emit(state.copyWith(resultMessage: "Save Successfully"));
      await Future.delayed(const Duration(milliseconds: 100));
      emit(state.copyWith(resultMessage: null));
    });
  }
}
