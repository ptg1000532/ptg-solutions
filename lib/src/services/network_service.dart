import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:ptg_solutions/src/constants/network_api.dart';
import 'package:ptg_solutions/src/models/profile.dart';
import 'package:ptg_solutions/src/models/work_time.dart';
import 'package:ptg_solutions/src/utils/common_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/user.dart';

// Singleton or Factory Design Pattern
class NetworkService {
  NetworkService._internal();

  static final NetworkService _instance = NetworkService._internal();

  factory NetworkService() => _instance;

  static final Dio _dio = Dio()
    ..interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) {
          options.baseUrl = NetworkAPI.baseURL;
          return handler.next(options);
        },
        onResponse: (response, handler) async {
          // await Future.delayed(Duration(seconds: 10));
          return handler.next(response);
        },
        onError: (DioError e, handler) {
          switch (e.response?.statusCode) {
            case 301:
              break;
            case 401:
              break;
            default:
          }
          return handler.next(e);
        },
      ),
    );

  Future<bool> login(User user) async {
    final response = await _dio.post(NetworkAPI.loginEndpoint, data: user);
    if (response.statusCode == 200) {
      return true;
    }
    throw Exception();
  }

  Future<Profile> getProfile(String? personCode) async {
    final url = "${NetworkAPI.profileEndpoint}?personCode=$personCode";
    final response = await _dio.get(url);
    if (response.statusCode == 200) {
      return profileFromJson(jsonEncode(response.data));
    }
    throw Exception();
  }

  Future<WorkTime> getTodayWorkTime(String? personCode) async {
    final url = "${NetworkAPI.getTodayWorkTimeEndpoint}?personCode=$personCode";
    final response = await _dio.get(url);
    if (response.statusCode == 200) {
      return workTimeFromJson(jsonEncode(response.data));
    }
    throw Exception();
  }

  Future<List<WorkTime>> getMonthWorkTime({required DateTime selectedMonth}) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      final personCode = prefs.getString(NetworkAPI.personCode);
      final selectedMonthString = formatDate(dateTime: selectedMonth, newFormat: "yyyy-MM-dd");
      print("CMDebug: ${selectedMonthString}");
      final url = "${NetworkAPI.getMonthWorkTimeEndpoint}?personCode=$personCode&selectedMonth=$selectedMonthString";
      final response = await _dio.get(url);
      if (response.statusCode == 200) {
        return allWorkTimeFromJson(jsonEncode(response.data));
      }
      return [];
      // throw Exception();
    } catch (e) {
      print(e.toString());
      return [];
    }
  }

  Future<List<WorkTime>> getAllWorkTime({DateTime? selectedDate}) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      final personCode = prefs.getString(NetworkAPI.personCode);
      final url = "${NetworkAPI.getAllWorkTimeEndpoint}?personCode=$personCode";
      final response = await _dio.get(url);
      if (response.statusCode == 200) {
        return allWorkTimeFromJson(jsonEncode(response.data));
      }
      return [];
      // throw Exception();
    } catch (e) {
      print(e.toString());
      return [];
    }
  }

  Future<WorkTime> saveWorkTime(
    String personCode,
    String location,
    String type,
  ) async {
    final dataMap = {
      'personCode': personCode,
      'location': location,
      'type': type,
    };

    final data = json.encode(dataMap);
    final response = await _dio.post(NetworkAPI.saveWorkTimeEndpoint, data: data);
    if (response.statusCode == 200) {
      return workTimeFromJson(jsonEncode(response.data));
    }
    throw Exception();
  }
}
